﻿using Helmes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Helmes.Utilities
{
    public class Utility
    {
      private List<SelectListItem> selectList = new List<SelectListItem>();
      string tab = string.Empty;
      public List<SelectListItem> getSelectList(List<Categories> categories, int parentId, int depth=0)
        {
            var items = categories.Where(d => d.ParentId == parentId).ToList();
            tab = string.Empty;
            if (items.Any())
            {
                foreach (var item in items)
                {
                    for (int i = 0; i < depth; i++)
                    {
                        tab += "&nbsp;&nbsp;&nbsp;&nbsp;";
                    }
                    selectList.Add(new SelectListItem() { Text =WebUtility.HtmlDecode(tab) + item.CategoryName, Value = item.Id.ToString() });
                   
                    getSelectList(categories, item.Id,depth+1);
                }
            }
            return selectList;

        }
    }
}