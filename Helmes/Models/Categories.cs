﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Helmes.Models
{
    public class Categories
    {
        public Categories() { }
        public Categories(int id, int parentId, string categoryName)
        {
            Id = id;
            ParentId = parentId;
            CategoryName = categoryName;
        }
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public int? ParentId { get; set; }
    }
}