﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Helmes.Models
{
    public class SectorItems
    {
        public int Id { get; set; }
        public string SectorName { get; set; }
        public int ParentId { get; set; }
        public List<SectorItems> Children { get; set; }
    }
}