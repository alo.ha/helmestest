﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Helmes.Models
{
    public class HelmesAgree
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int SectorID { get; set; }
        public bool AgreeToTerms { get; set; }
    }
}