﻿using Helmes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Helmes.ViewModel
{
    public class HomeViewModel
    {
        public IEnumerable<SelectListItem> SectorItems { get; set; }
        public int agreedID { get; set; }
        public string AgreedName { get; set; }
        public bool AgreeToTerms { get; set; }
        public string AgreedSector { get; set; }
        public HelmesAgree AgreedItem { get; set; }
    }
}