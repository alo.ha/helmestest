namespace Helmes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Fixdb : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.HelmesAgrees", newName: "HelmesAgree");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.HelmesAgree", newName: "HelmesAgrees");
        }
    }
}
