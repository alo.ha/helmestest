namespace Helmes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixedsectorID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HelmesAgree", "SectorID", c => c.Int(nullable: false));
            DropColumn("dbo.HelmesAgree", "Sector");
        }
        
        public override void Down()
        {
            AddColumn("dbo.HelmesAgree", "Sector", c => c.String());
            DropColumn("dbo.HelmesAgree", "SectorID");
        }
    }
}
