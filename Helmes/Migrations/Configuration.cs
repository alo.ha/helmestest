namespace Helmes.Migrations
{
    using Helmes.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Helmes.DAL.HelmesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Helmes.DAL.HelmesContext context)
        {
            context.Categories.AddOrUpdate(x => x.Id,
                new Categories() { Id=0},
                new Categories() {Id=1, ParentId=0, CategoryName= "Manufacturing"},
                new Categories() {Id=2, ParentId=0 ,CategoryName = "Other" },
                new Categories() {Id=3, ParentId=0, CategoryName = "Service" },
                new Categories() { Id = 4,ParentId=1, CategoryName = "Construction materials" },
                new Categories() { Id = 5, ParentId = 1, CategoryName = "Electronics and Optics" },
                new Categories() { Id = 6, ParentId = 1, CategoryName = "Food and Beverage" },
                new Categories() { Id = 7, ParentId = 1, CategoryName = "Furniture" },
                new Categories() { Id = 8, ParentId = 1, CategoryName = "Machinery" },
                new Categories() { Id = 9, ParentId = 1, CategoryName = "Metalworking" },
                new Categories() { Id = 10, ParentId = 1, CategoryName = "Plastic and Rubber" },
                new Categories() { Id = 11, ParentId = 1, CategoryName = "Printing" },
                new Categories() { Id = 12, ParentId = 1, CategoryName = "Textile and Clothing" },
                new Categories() { Id = 13, ParentId = 1, CategoryName = "Wood" },
                new Categories() { Id = 14, ParentId = 2, CategoryName = "Creative industries" },
                new Categories() { Id = 15, ParentId = 2, CategoryName = "Energy technology" },
                new Categories() { Id = 16, ParentId = 2, CategoryName = "Environment" },
                new Categories() { Id = 17, ParentId = 3, CategoryName = "Business services" },
                new Categories() { Id = 18, ParentId = 3, CategoryName = "Engineering" },
                new Categories() { Id = 19, ParentId = 3, CategoryName = "Information Technology and Telecommunications" },
                new Categories() { Id = 20, ParentId = 3, CategoryName = "Tourism" },
                new Categories() { Id = 21, ParentId = 3, CategoryName = "Translation services" },
                new Categories() { Id = 22, ParentId = 3, CategoryName = "Transport and Logistics" },
                new Categories() { Id = 23, ParentId = 6, CategoryName = "Bakery & confectionery products" },
                new Categories() { Id = 24, ParentId = 6, CategoryName = "Beverages" },
                new Categories() { Id = 25, ParentId = 6, CategoryName = "Fish & fish products" },
                new Categories() { Id = 26, ParentId = 6, CategoryName = "Meat & meat products" },
                new Categories() { Id = 27, ParentId = 6, CategoryName = "Milk & dairy products" },
                new Categories() { Id = 28, ParentId = 6, CategoryName = "Other" },
                new Categories() { Id = 29, ParentId = 6, CategoryName = "Sweets & snack food" },
                new Categories() { Id = 30, ParentId = 7, CategoryName = "Bathroom/sauna" },
                new Categories() { Id = 31, ParentId = 7, CategoryName = "Bedroom" },
                new Categories() { Id = 32, ParentId = 7, CategoryName = "Children�s room" },
                new Categories() { Id = 33, ParentId = 7, CategoryName = "Kitchen" },
                new Categories() { Id = 34, ParentId = 7, CategoryName = "Living room" },
                new Categories() { Id = 35, ParentId = 7, CategoryName = "Office" },
                new Categories() { Id = 36, ParentId = 7, CategoryName = "Other (Furniture)" },
                new Categories() { Id = 37, ParentId = 7, CategoryName = "Outdoor" },
                new Categories() { Id = 38, ParentId = 7, CategoryName = "Project furniture" },
                new Categories() { Id = 39, ParentId = 8, CategoryName = "Machinery components" },
                new Categories() { Id = 40, ParentId = 8, CategoryName = "Machinery equipment/tools" },
                new Categories() { Id = 41, ParentId = 8, CategoryName = "Manufacture of machinery 	" },
                new Categories() { Id = 42, ParentId = 8, CategoryName = "Maritime" },
                new Categories() { Id = 43, ParentId = 8, CategoryName = "Metal structures" },
                new Categories() { Id = 44, ParentId = 8, CategoryName = "Other" },
                new Categories() { Id = 45, ParentId = 8, CategoryName = "Repair and maintenance service" },
                new Categories() { Id = 46, ParentId = 9, CategoryName = "Construction of metal structures" },
                new Categories() { Id = 47, ParentId = 9, CategoryName = "Houses and buildings" },
                new Categories() { Id = 48, ParentId = 9, CategoryName = "Metal products" },
                new Categories() { Id = 49, ParentId = 9, CategoryName = "Metal works" },
                new Categories() { Id = 50, ParentId = 10, CategoryName = "Packaging" },
                new Categories() { Id = 51, ParentId = 10, CategoryName = "Plastic goods" },
                new Categories() { Id = 52, ParentId = 10, CategoryName = "Plastic processing technology" },
                new Categories() { Id = 53, ParentId = 10, CategoryName = "Plastic profiles" },
                new Categories() { Id = 54, ParentId = 11, CategoryName = "Advertising" },
                new Categories() { Id = 55, ParentId = 11, CategoryName = "Book/Periodicals printing" },
                new Categories() { Id = 56, ParentId = 11, CategoryName = "Labelling and packaging printing" },
                new Categories() { Id = 57, ParentId = 12, CategoryName = "Clothing" },
                new Categories() { Id = 58, ParentId = 12, CategoryName = "Textile" },
                new Categories() { Id = 59, ParentId = 13, CategoryName = "Other (Wood)" },
                new Categories() { Id = 60, ParentId = 13, CategoryName = "Wooden building materials" },
                new Categories() { Id = 61, ParentId = 13, CategoryName = "Wooden houses" },
                new Categories() { Id = 62, ParentId = 19, CategoryName = "Data processing, Web portals, E-marketing" },
                new Categories() { Id = 63, ParentId = 19, CategoryName = "Programming, Consultancy" },
                new Categories() { Id = 64, ParentId = 19, CategoryName = "Software, Hardware" },
                new Categories() { Id = 65, ParentId = 19, CategoryName = "Telecommunications" },
                new Categories() { Id = 66, ParentId = 22, CategoryName = "Air" },
                new Categories() { Id = 67, ParentId = 22, CategoryName = "Rail" },
                new Categories() { Id = 68, ParentId = 22, CategoryName = "Road" },
                new Categories() { Id = 69, ParentId = 22, CategoryName = "Water" },
                new Categories() { Id = 70, ParentId = 42, CategoryName = "Aluminium and steel workboats" },
                new Categories() { Id = 71, ParentId = 42, CategoryName = "Boat/Yacht building" },
                new Categories() { Id = 72, ParentId = 42, CategoryName = "Ship repair and conversion" },
                new Categories() { Id = 73, ParentId = 49, CategoryName = "CNC-machining" },
                new Categories() { Id = 74, ParentId = 49, CategoryName = "Forgings, Fasteners" },
                new Categories() { Id = 75, ParentId = 49, CategoryName = "Gas, Plasma, Laser cutting" },
                new Categories() { Id = 76, ParentId = 49, CategoryName = "MIG, TIG, Aluminum welding" },
                new Categories() { Id = 77, ParentId = 52, CategoryName = "Blowing" },
                new Categories() { Id = 78, ParentId = 52, CategoryName = "Moulding" },
                new Categories() { Id = 79, ParentId = 52, CategoryName = "Plastics welding and processing" }
                                );
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

        }
    }
}
