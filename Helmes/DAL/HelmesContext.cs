﻿using Helmes.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Helmes.DAL
{
    public class HelmesContext : DbContext
    {
        public HelmesContext() : base() { }

        public DbSet<Categories> Categories { get; set; }
        public DbSet<HelmesAgree> HelmesAgree { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}