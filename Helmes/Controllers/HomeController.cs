﻿using Helmes.DAL;
using Helmes.Models;
using Helmes.Utilities;
using Helmes.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Helmes.Controllers
{
    public class HomeController : Controller
    {
        private HelmesContext dbContext = new HelmesContext();
        Utility utility = new Utility();
        HomeViewModel VM=new HomeViewModel();
        // GET: Home
        public ActionResult Index(int? id)
        {
            var categories = utility.getSelectList(dbContext.Categories.ToList(), 0);
            var agreedItem = dbContext.HelmesAgree.Where(x => x.ID == id).FirstOrDefault();            
            if (id!=null)
            {
                VM = new HomeViewModel()
                {
                    agreedID = agreedItem.ID,
                    AgreedName = agreedItem.Name,
                    AgreedSector = agreedItem.SectorID.ToString(),
                    AgreeToTerms = agreedItem.AgreeToTerms
                };
            }
           
            VM.SectorItems = categories;
            return View(VM);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Save([Bind(Include = "agreedID,AgreedName,AgreeToTerms,AgreedSector")] HomeViewModel _agreedItem)
        {
            HelmesAgree agreedItem;
            if (ModelState.IsValid)
            {
                    var sectorID = int.Parse(_agreedItem.AgreedSector);
                    agreedItem = new HelmesAgree()
                    {
                        Name = _agreedItem.AgreedName,
                        AgreeToTerms = _agreedItem.AgreeToTerms,
                        SectorID = sectorID
                    };
                if (_agreedItem.agreedID!=0)
                {
                    agreedItem.ID = _agreedItem.agreedID;
                    dbContext.Entry(agreedItem).State = EntityState.Modified;
                }
                else
                {
                    dbContext.HelmesAgree.Add(agreedItem);
                }
                dbContext.SaveChanges();
                return RedirectToAction("Index", "Home", new { id = agreedItem.ID });
            }
            return View();
        }
    }
}